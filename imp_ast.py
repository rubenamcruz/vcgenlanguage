# Copyright (c) 2011, Jay Conrod.
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Jay Conrod nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL JAY CONROD BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from equality import *
import z3


# region Obtained Code

class Statement(Equality):
    pass

class Aexp(Equality):
    pass

class Bexp(Equality):
    pass

class VCexp(Equality):
    pass

class AssignStatement(Statement):
    def __init__(self, name, aexp):
        self.name = name
        self.aexp = aexp
        self.local_env = {}

    def __repr__(self):
        return 'AssignStatement(%s, %s)' % (self.name, self.aexp)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        if self.name not in self.local_env.keys():
            self.local_env[self.name] = z3.Int(self.name)
        self.aexp.first_eval(self.local_env,oldies, False)
        return self.local_env

    def second_eval(self, vcs, oldies, post):
        value = self.aexp.second_eval(vcs,oldies, post)
        print value
        if type(value[0]) == int:
            p1 = z3.substitute(post[0], (self.local_env[self.name], self.local_env[self.name] - self.local_env[self.name] + value[0]))
            p2 = z3.substitute(post[1], (self.local_env[self.name], self.local_env[self.name] - self.local_env[self.name] + value[1]))
        else:
            p1 = z3.substitute(post[0], (self.local_env[self.name], value[0]))
            p2 = z3.substitute(post[1], (self.local_env[self.name], value[1]))
        return p1, p2


class CompoundStatement(Statement):
    def __init__(self, first, second):
        self.first = first
        self.second = second
        self.local_env = {}

    def __repr__(self):
        return 'CompoundStatement(%s, %s)' % (self.first, self.second)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        left_env = self.first.first_eval(self.local_env,oldies, can_add)
        for var in left_env.keys():
            self.local_env[var] = left_env[var]
        right_env = self.second.first_eval(self.local_env,oldies, can_add)
        for var in right_env.keys():
            self.local_env[var] = right_env[var]
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        second_eval = self.second.second_eval(vcs,oldies, post)
        first_eval = self.first.second_eval(vcs,oldies, second_eval)
        return first_eval



class IfStatement(Statement):
    def __init__(self, condition, true_stmt, false_stmt):
        self.condition = condition
        self.true_stmt = true_stmt
        self.false_stmt = false_stmt
        self.local_env = {}

    def __repr__(self):
        return 'IfStatement(%s, %s, %s)' % (self.condition, self.true_stmt, self.false_stmt)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.condition.first_eval(self.local_env,oldies, False)
        self.true_stmt.first_eval(self.local_env,oldies, True)
        if not self.false_stmt is None:
            self.false_stmt.first_eval(self.local_env,oldies, True)
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        condition_value = self.condition.second_eval(vcs,oldies, post)
        true_value = self.true_stmt.second_eval(vcs,oldies, post)
        false_value = post
        if not self.false_stmt is None:
            false_value = self.false_stmt.second_eval(vcs,oldies, post)

        p1 = None
        p2 = None
        if str(true_value[0]) != str(z3.Int('the_great_big_variable_to_define_true_or_false') != z3.Int('the_great_big_variable_to_define_true_or_false')) and str(false_value[0]) != str(z3.Int('the_great_big_variable_to_define_true_or_false') != z3.Int('the_great_big_variable_to_define_true_or_false')):
            p1 = z3.And(z3.Implies(condition_value[0], true_value[0]),
                        z3.Implies(z3.Not(condition_value[0]), false_value[0]))
        elif str(true_value[0]) != str(z3.Int('the_great_big_variable_to_define_true_or_false') != z3.Int('the_great_big_variable_to_define_true_or_false')):
            p1 = z3.Implies(condition_value[0], true_value[0])
        else:
            p1 = z3.Implies(z3.Not(condition_value[0]), false_value[0])

        if str(true_value[1]) != str(z3.Int('the_great_big_variable_to_define_true_or_false') != z3.Int(
                'the_great_big_variable_to_define_true_or_false')) and str(false_value[1]) != str(
                        z3.Int('the_great_big_variable_to_define_true_or_false') != z3.Int(
                        'the_great_big_variable_to_define_true_or_false')):
            p2 = z3.And(z3.Implies(condition_value[1], true_value[1]),
                        z3.Implies(z3.Not(condition_value[1]), false_value[1]))
        elif str(true_value[1]) != str(z3.Int('the_great_big_variable_to_define_true_or_false') != z3.Int(
                'the_great_big_variable_to_define_true_or_false')):
            p2 = z3.Implies(condition_value[1], true_value[1])
        else:
            p2 = z3.Implies(z3.Not(condition_value[1]), false_value[1])

        return (p1, p2)


class WhileStatement(Statement):
    def __init__(self, condition, body, invariant = None):
        self.condition = condition
        self.body = body
        self.invariant = invariant
        self.local_env = {}

    def __repr__(self):
                # x() returns the Z3 expression
                # x.name() returns a string:
        return 'WhileStatement(%s, %s, %s)' % (self.condition, self.invariant, self.body)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.condition.first_eval(self.local_env,oldies, False)
        self.body.first_eval(self.local_env,oldies, True)
        self.invariant.first_eval(self.local_env,oldies, False)
        return self.local_env


    def second_eval(self, vcs,oldies, post):
        condition_value = self.condition.second_eval(vcs,oldies, post)
        invariant_value = self.invariant.second_eval(vcs,oldies, post)
        body_value = self.body.second_eval(vcs,oldies, invariant_value)
        final_value = None
        if str(post[0]) == str(z3.Int('the_great_big_variable_to_define_true_or_false') !=
                                       z3.Int('the_great_big_variable_to_define_true_or_false')):
            vcs.append(z3.Not(z3.Implies(z3.And(invariant_value[1], z3.Not(condition_value[1])), post[1])))
            final_value = (post[0], invariant_value[1])
        elif str(post[1]) == str(z3.Int('the_great_big_variable_to_define_true_or_false') !=
                                       z3.Int('the_great_big_variable_to_define_true_or_false')):
            vcs.append(z3.Not(z3.Implies(z3.And(invariant_value[0], z3.Not(condition_value[0])), post[0])))
            final_value = (invariant_value[0], post[1])
        else:
            vcs.append(z3.Not(z3.Implies(z3.And(invariant_value[0], z3.Not(condition_value[0])), post[0])))
            vcs.append(z3.Not(z3.Implies(z3.And(invariant_value[1], z3.Not(condition_value[0])), post[1])))
            final_value = invariant_value
        if str(body_value[0]) != str(z3.Int('the_great_big_variable_to_define_true_or_false') !=
                                       z3.Int('the_great_big_variable_to_define_true_or_false')):
            vcs.append(z3.Not(z3.Implies(z3.And(invariant_value[0], condition_value[0]), body_value[0])))
        if str(body_value[1]) != str(z3.Int('the_great_big_variable_to_define_true_or_false') !=
                                             z3.Int('the_great_big_variable_to_define_true_or_false')):
            vcs.append(z3.Not(z3.Implies(z3.And(invariant_value[1], condition_value[1]), body_value[1])))

        return final_value

class IntAexp(Aexp):
    def __init__(self, i):
        self.i = i
        self.local_env = {}

    def __repr__(self):
        return 'IntAexp(%d)' % self.i

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        return self.local_env

    def second_eval(self, vcs,oldies, _):
        return (self.i, self.i)


class VarAexp(Aexp):
    def __init__(self, name):
        self.name = name
        self.local_env = {}

    def __repr__(self):
        return 'VarAexp(%s)' % self.name

    def first_eval(self, global_env, oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        if can_add and (self.name not in self.local_env.keys()):
            self.local_env[self.name] = z3.Int(self.name)
        elif not can_add and self.name not in self.local_env.keys():
            raise RuntimeError('Undefined variable %s' % (self.name))
        return self.local_env


    def second_eval(self, vcs,oldies, _):
        return (self.local_env[self.name], self.local_env[self.name])


class BinopAexp(Aexp):
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right
        self.local_env = {}

    def __repr__(self):
        return 'BinopAexp(%s, %s, %s)' % (self.op, self.left, self.right)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.left.first_eval(self.local_env,oldies, can_add)
        self.right.first_eval(self.local_env,oldies, can_add)
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        left_value = self.left.second_eval(vcs,oldies, post)[0]
        right_value = self.right.second_eval(vcs,oldies, post)[0]
        if self.op == '+':
            value = left_value + right_value
        elif self.op == '-':
            value = left_value - right_value
        elif self.op == '*':
            value = left_value * right_value
        elif self.op == '/':
            value = left_value / right_value
        else:
            raise RuntimeError('unknown operator: ' + self.op)
        return (value, value)

class RelopBexp(Bexp):
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right
        self.local_env = {}

    def __repr__(self):
        return 'RelopBexp(%s, %s, %s)' % (self.op, self.left, self.right)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.left.first_eval(self.local_env,oldies, can_add)
        self.right.first_eval(self.local_env,oldies, can_add)
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        left_value = self.left.second_eval( vcs,oldies, post)[0]
        right_value = self.right.second_eval( vcs,oldies, post)[0]
        if self.op == '<':
            value = left_value < right_value
        elif self.op == '<=':
            value = left_value <= right_value
        elif self.op == '>':
            value = left_value > right_value
        elif self.op == '>=':
            value = left_value >= right_value
        elif self.op == '=':
            value = left_value == right_value
        elif self.op == '!=':
            value = left_value != right_value
        else:
            raise RuntimeError('unknown operator: ' + self.op)
        return (value, value)

class AndBexp(Bexp):
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.local_env = {}

    def __repr__(self):
        return 'AndBexp(%s, %s)' % (self.left, self.right)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.left.first_eval(self.local_env,oldies, can_add)
        self.right.first_eval(self.local_env,oldies, can_add)
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        left_value = self.left.second_eval(vcs,oldies, post)
        right_value = self.right.second_eval(vcs,oldies, post)
        return (z3.And(left_value[0], right_value[0]), z3.And(left_value[1], right_value[1]))

class OrBexp(Bexp):
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.local_env = {}

    def __repr__(self):
        return 'OrBexp(%s, %s)' % (self.left, self.right)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.left.first_eval(self.local_env,oldies, can_add)
        self.right.first_eval(self.local_env,oldies, can_add)
        return self.local_env

    def second_eval(self, vcs, oldies,post):
        left_value = self.left.second_eval( vcs,oldies, post)
        right_value = self.right.second_eval(vcs,oldies, post)
        return (z3.Or(left_value[0], right_value[0]), z3.Or(left_value[1], right_value[1]))


class NotBexp(Bexp):
    def __init__(self, exp):
        self.exp = exp
        self.local_env = {}

    def __repr__(self):
        return 'NotBexp(%s)' % self.exp

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.exp.first_eval(self.local_env,oldies, can_add)

    def second_eval(self, vcs,oldies, post):
        value = self.exp.second_eval(vcs,oldies, post)
        return (z3.Not(value[0]), z3.Not(value[1]))


# endregion

# region MyCode


class Throw_Statement(Statement):
    def __init__(self, throw):
        self.throw = throw
        self.local_env = {}

    def __repr__(self):
        return 'Throw_Statement'

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        if not 'the_great_big_variable_to_define_true_or_false' in self.local_env.keys():
            self.local_env['the_great_big_variable_to_define_true_or_false'] = z3.Int(
                'the_great_big_variable_to_define_true_or_false')
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        return (self.local_env['the_great_big_variable_to_define_true_or_false'] != self.local_env['the_great_big_variable_to_define_true_or_false'], post[1])

class Try_Catch_Statement(Statement):
    def __init__(self, try_statement, catch_statement, finally_statement):
        self.try_statement = try_statement
        self.catch_statement = catch_statement
        self.finally_statement = finally_statement
        self.local_env = {}

    def __repr__(self):
        return 'Try_Catch_Statement (%s, %s, %s)' % (self.try_statement, self.catch_statement, self.finally_statement)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.try_statement.first_eval(self.local_env,oldies, can_add)
        self.catch_statement.first_eval(self.local_env,oldies, can_add)
        if not self.finally_statement  is None:
            self.finally_statement.first_eval(self.local_env,oldies, can_add)
        return self.local_env

    def second_eval(self,vcs,oldies, post):
        if not self.finally_statement is None:
            post = self.finally_statement.second_eval( vcs,oldies, post)
        value_catch = self.catch_statement.second_eval( vcs,oldies, (
            z3.Int('the_great_big_variable_to_define_true_or_false') != z3.Int('the_great_big_variable_to_define_true_or_false'), post[1]))
        value_try = self.try_statement.second_eval( vcs,oldies, (post[0], z3.Int('the_great_big_variable_to_define_true_or_false') !=
                                       z3.Int('the_great_big_variable_to_define_true_or_false')))
        return (value_try[0], value_catch[1])

class Assertion_Statement(Statement):
    def __init__(self, condition):
        self.condition = condition
        self.local_env = {}

    def __repr__(self):
        return 'Assertion_Statement (%s)' % self.condition


    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.condition.first_eval(self.local_env,oldies, False)
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        value = self.condition.second_eval(vcs,oldies, post)[0]
        final_value = None
        if str(post[0]) == str(z3.Int('the_great_big_variable_to_define_true_or_false') != z3.Int(
            'the_great_big_variable_to_define_true_or_false')):
            vcs.append(z3.Not(z3.Implies(value, post[1])))
            final_value = (post[0], value)
        elif str(post[1]) == str(z3.Int('the_great_big_variable_to_define_true_or_false') != z3.Int(
            'the_great_big_variable_to_define_true_or_false')):
            vcs.append(z3.Not(z3.Implies(value, post[0])))
            final_value = ( value, post[1])
        else:
            vcs.append(z3.Not(z3.Implies(value, post[0])))
            vcs.append(z3.Not(z3.Implies(value, post[1])))
            final_value = (value, value)

        return final_value


class Precondition_Statement(Statement):
    def __init__(self, condition):
        self.condition = condition
        self.local_env = {}

    def __repr__(self):
        return 'Precondition_Statement (%s)' % self.condition


    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        result = self.condition.first_eval(self.local_env,oldies, True)
        for var in result.keys():
            self.local_env[var] = result[var]
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        value = self.condition.second_eval(vcs,oldies, post)[0]
        if str(post[0]) == str(z3.Int('the_great_big_variable_to_define_true_or_false') !=
                                       z3.Int('the_great_big_variable_to_define_true_or_false')):
            vcs.append(z3.Not(z3.Implies(value, post[1])))
        elif str(post[1]) == str(z3.Int('the_great_big_variable_to_define_true_or_false') !=
                                       z3.Int('the_great_big_variable_to_define_true_or_false')):
            vcs.append(z3.Not(z3.Implies(value, post[0])))
        else:
            vcs.append(z3.Not(z3.Implies(value, post[0])))
            vcs.append(z3.Not(z3.Implies(value, post[1])))




class Postcondition_Statement(Statement):
    def __init__(self, condition_true, condition_false):
        self.condition_true = condition_true
        self.condition_false = condition_false
        self.local_env = {}

    def __repr__(self):
        return 'Postcondition_Statement (%s, %s)' % (self.condition_true, self.condition_false)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        self.condition_true.first_eval(self.local_env,oldies, False)
        self.condition_false.first_eval(self.local_env,oldies, False)
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        value_true = self.condition_true.second_eval(vcs,oldies, post)
        value_false = self.condition_false.second_eval(vcs,oldies, post)
        return (value_true[0], value_false[1])


class NotVCexp(VCexp):
    def __init__(self, exp):
        self.exp = exp
        self.local_env = {}

    def __repr__(self):
        return 'NotVCexp(%s)' % self.exp

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        env = self.exp.first_eval(self.local_env,oldies, can_add)
        if can_add:
            for var in env.keys():
                self.local_env[var] = env[var]
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        value = self.exp.second_eval(vcs,oldies, post)
        return (z3.Not(value[0]), z3.Not(value[1]))


class RelopVCexp(VCexp):
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right
        self.local_env = {}

    def __repr__(self):
        return 'RelopVCexp(%s, %s, %s)' % (self.op, self.left, self.right)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        left_env = self.left.first_eval(self.local_env,oldies, can_add)
        right_env = self.right.first_eval(self.local_env,oldies, can_add)
        if can_add:
            for var in left_env.keys():
                self.local_env[var] = left_env[var]
            for var in right_env.keys():
                self.local_env[var] = right_env[var]
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        left_value = self.left.second_eval( vcs,oldies, post)[0]
        right_value = self.right.second_eval(vcs,oldies, post)[0]
        if self.op == '<':
            value = left_value < right_value
        elif self.op == '<=':
            value = left_value <= right_value
        elif self.op == '>':
            value = left_value > right_value
        elif self.op == '>=':
            value = left_value >= right_value
        elif self.op == '=':
            value = left_value == right_value
        elif self.op == '!=':
            value = left_value != right_value
        else:
            raise RuntimeError('unknown operator: ' + self.op)
        return (value, value)

class AndVCexp(VCexp):
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.local_env = {}

    def __repr__(self):
        return 'AndVCexp(%s, %s)' % (self.left, self.right)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        left_env = self.left.first_eval(self.local_env,oldies, can_add)
        right_env = self.right.first_eval(self.local_env,oldies, can_add)
        if can_add:
            for var in left_env.keys():
                self.local_env[var] = left_env[var]
            for var in right_env.keys():
                self.local_env[var] = right_env[var]
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        left_value = self.left.second_eval( vcs,oldies, post)
        right_value = self.right.second_eval(vcs,oldies, post)
        return (z3.And(left_value[0], right_value[0]), z3.And(left_value[1], right_value[1]))

class OrVCexp(VCexp):
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.local_env = {}

    def __repr__(self):
        return 'OrVCexp(%s, %s)' % (self.left, self.right)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        left_env = self.left.first_eval(self.local_env,oldies, can_add)
        right_env = self.right.first_eval(self.local_env,oldies, can_add)
        if can_add:
            for var in left_env.keys():
                self.local_env[var] = left_env[var]
            for var in right_env.keys():
                self.local_env[var] = right_env[var]
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        left_value = self.left.second_eval( vcs,oldies, post)
        right_value = self.right.second_eval(vcs,oldies, post)
        return (z3.Or(left_value[0], right_value[0]), z3.Or(left_value[1], right_value[1]))




class ImpliesVCexp(VCexp):
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.local_env = {}

    def __repr__(self):
        return 'OrVCexp(%s, %s)' % (self.left, self.right)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        left_env = self.left.first_eval(self.local_env,oldies, can_add)
        right_env = self.right.first_eval(self.local_env,oldies, can_add)
        if can_add:
            for var in left_env.keys():
                self.local_env[var] = left_env[var]
            for var in right_env.keys():
                self.local_env[var] = right_env[var]
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        left_value = self.left.second_eval(vcs,oldies, post)
        right_value = self.right.second_eval(vcs,oldies, post)
        return (z3.Implies(left_value[0], right_value[0]), z3.Implies(left_value[1], right_value[1]))




class QuantifierVCExp(VCexp):
    def __init__(self,quantifier, vars, exp):
        self.exp = exp
        self.quantifier = quantifier
        self.vars = vars
        self.local_env = {}

    def __repr__(self):
        return 'QuantifierVCexp(%s, %s, %s)' % (self.quantifier, self.vars, self.exp)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        quantifiers_env = self.vars.first_eval(self.local_env,oldies, True)
        self.exp.first_eval(quantifiers_env,oldies, False)
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        value_vars = self.vars.second_eval(vcs,oldies, post)
        value_exp = self.exp.second_eval(vcs,oldies, post)
        if self.quantifier == 'forall':
            return (z3.ForAll(value_vars[0], value_exp[0]), z3.ForAll(value_vars[1], value_exp[1]))
        elif self.quantifier == 'exists':
            return (z3.Exists(value_vars[0], value_exp[0]), z3.Exists(value_vars[1], value_exp[1]))
        else:
            raise RuntimeError('Error: quantifier %s not valid' % self.quantifier)


class AtomVCexp(VCexp):
    def __init__(self, atom):
        self.atom = atom
        self.local_env = {}

    def __repr__(self):
        return 'AtomVCexp(%s)' % (self.atom)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        if not 'the_great_big_variable_to_define_true_or_false' in self.local_env.keys():
            self.local_env['the_great_big_variable_to_define_true_or_false'] = z3.Int('the_great_big_variable_to_define_true_or_false')
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        if self.atom == 'true':
            return (self.local_env['the_great_big_variable_to_define_true_or_false'] == self.local_env['the_great_big_variable_to_define_true_or_false'],self.local_env['the_great_big_variable_to_define_true_or_false'] == self.local_env['the_great_big_variable_to_define_true_or_false'])
        elif self.atom == 'false':
            return (self.local_env['the_great_big_variable_to_define_true_or_false'] != self.local_env['the_great_big_variable_to_define_true_or_false'], self.local_env['the_great_big_variable_to_define_true_or_false'] != self.local_env['the_great_big_variable_to_define_true_or_false'])
        else:
            raise RuntimeError('Error: atom %s not valid' % self.atom)



class VarsExp(VCexp):
    def __init__(self, l, r):
        self.l = l
        self.r = r
        self.local_env = {}

    def __repr__(self):
        return 'VarsExp (%s, %s)' %(self.l, self.r)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        left_env = self.l.first_eval(self.local_env,oldies, can_add)
        for var in left_env.keys():
            self.local_env[var] = left_env[var]
        right_env = self.r.first_eval(self.local_env,oldies, can_add)
        for var in right_env.keys():
            self.local_env[var] = right_env[var]
        return self.local_env

    def second_eval(self,vcs,oldies, post):
        left_value = self.l.second_eval( vcs,oldies, post)
        right_value = self.r.second_eval(vcs,oldies, post)
        return (left_value[0] + right_value[0], left_value[1] + right_value[1])


class VarVCexp(VCexp):
    def __init__(self, name):
        self.name = name
        self.local_env = {}

    def __repr__(self):
        return 'VarVCExp (%s)' %(self.name)

    def first_eval(self, global_env,oldies, can_add):
        for var in global_env.keys():
            self.local_env[var] = global_env[var]
        if can_add and (self.name not in global_env.keys()):
            self.local_env[self.name] = z3.Int(self.name)
        elif can_add and self.name in global_env.keys():
            raise RuntimeError('Linked variable already defined %s' % (self.name))
        elif not can_add and self.name not in global_env.keys():
            raise RuntimeError('Undefined variable %s' % (self.name))
        return self.local_env

    def second_eval(self, vcs,oldies, post):
        return ([self.local_env[self.name]], [self.local_env[self.name]])


# class OldVarExp(VCexp):
#     def __init__(self, name):
#         self.name = name
#
#     def __repr__(self):
#         return 'OldVarExp (%s)' %(self.name)
#
#     def first_eval(self, global_env, can_add):
#         if can_add and (self.name not in global_env.keys()):
#             global_env[self.name] = z3.Int(self.name)
#         elif not can_add and self.name not in global_env.keys():
#             raise RuntimeError('Undefined variable %s' % (self.name))
#
#
#
#     def second_eval(self, global_env, vcs, post):
#         return ([global_env[self.name]], [global_env[self.name]])
# endregion