# -*- coding: UTF-8 -*-

# Generator of a class that tests its equality to other classes. Just used as abstract class to the parser.
class Equality:
    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
               self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)
