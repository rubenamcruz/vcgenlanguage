#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# Copyright (c) 2011, Jay Conrod.
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Jay Conrod nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL JAY CONROD BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from imp_lexer import *
from combinators import *
from imp_ast import *

# Basic parsers
def keyword(kw):
    return Reserved(kw, RESERVED)

num = Tag(INT) ^ (lambda i: int(i))
id = Tag(ID)

# Top level parser
def imp_parse(tokens):
    ast = parser()(tokens, 0)
    return ast

# parses the entire program
def parser():
    return Phrase(precondition_stmt() + stmt_list() + postcondition_stmt())


# Statements

# parses the precondition statement
def precondition_stmt():
    def process(parsed):
        ((_, vc), _) = parsed
        return Precondition_Statement(vc)
    return keyword('pre') + vcexp() + keyword('end') ^ process


# parses the postcondition statement
def postcondition_stmt():
    def process(parsed):
        ((((_, vct), _),vcf), _) = parsed
        return Postcondition_Statement(vct, vcf)
    return keyword('post') + vcexp() + keyword(',') + vcexp() + keyword('end') ^ process


# parses a list of statements separated by the ; keyword. It can, after that list, try to parse the try catch statement
# if it exists in the program
def stmt_list():
    def process(parsed):
        (initial,tries) = parsed
        if tries is None:
            return initial
        else:
            (_, realtries) = tries
            return CompoundStatement(initial, realtries)
    separator = keyword(';') ^ (lambda x: lambda l, r: CompoundStatement(l, r))
    return (Exp(stmt(), separator) + Opt(keyword(';') + try_catch_stmt())) ^ process

# When parsing the statements before the try and catch, the statements must be one of these
def stmt():
    return assign_stmt() | \
           if_stmt()     | \
           while_stmt() | \
           assert_stmt()


# used inside the body of the try statement, it allows the use of the throw statement, to make us jump to the catch statement
def stmt_list2():
    separator = keyword(';') ^ (lambda x: lambda l, r: CompoundStatement(l, r))
    return Exp((stmt2()), separator)

# when we can't have  the try statement (example: inside while, if, catch, and finally  bodies) we use this parser to
# parse lists of statements
def stmt_list3():
    separator = keyword(';') ^ (lambda x: lambda l, r: CompoundStatement(l, r))
    return Exp(stmt(), separator)

# used inside the body of the try, parses one of the statements of the language (with the exception of the try catch statement
def stmt2():
    return assign_stmt() | \
           if_stmt(stmt_list2)     | \
           while_stmt(stmt_list2) | \
           assert_stmt() | \
           throw_stmt()


# this parser parses the try and catch statement. If it can parse the finally keyword, there is a finally_statement body
def try_catch_stmt():
    def process(parsed):
        (((((_, try_statement), _), catch_statement),finally_statement), _) = parsed
        if finally_statement:
            (_, finally_statement) = finally_statement
        else:
            finally_statement = None
        return Try_Catch_Statement(try_statement, catch_statement, finally_statement)
    return keyword('try') + Lazy(stmt_list2) + \
           keyword('catch') + Lazy(stmt_list3) + \
           Opt(keyword('finally') + Lazy(stmt_list3)) + \
           keyword('end') ^ process


# parses the throw statement
def throw_stmt():
    return keyword('throw') ^ (lambda x: Throw_Statement(x))

# parses an assertion statement
def assert_stmt():
    def process(parsed):
        (_, vc) = parsed
        return Assertion_Statement(vc)
    return keyword('assert') + vcexp() ^ process

# parses an assignment statement
def assign_stmt():
    def process(parsed):
        ((name, _), exp) = parsed
        return AssignStatement(name, exp)
    return id + keyword(':=') + aexp() ^ process


# parses an if statement. It receives a function of the list of statements that are allowed in its body
def if_stmt(func = stmt_list3):
    def process(parsed):
        (((((_, condition), _), true_stmt), false_parsed), _) = parsed
        if false_parsed:
            (_, false_stmt) = false_parsed
        else:
            false_stmt = None
        return IfStatement(condition, true_stmt, false_stmt)
    return keyword('if') + bexp() + \
           keyword('then') + Lazy(func) + \
           Opt(keyword('else') + Lazy(func)) + \
           keyword('end') ^ process


# parses an while loop. It receives a function of the list of statements that are allowed in its body
def while_stmt(func = stmt_list3):
    def process(parsed):
        (((((((_, condition), _),_),invariant), _), body), _) = parsed
        return WhileStatement(condition, body, invariant)
    return keyword('while') + bexp() + \
           keyword('do') + keyword('{') + vcexp() + keyword('}') + \
           Lazy(func) + \
           keyword('end') ^ process

# verification condition expressions

#parses an verification condition expression, giving the right precedence levels
def vcexp():
    return precedence(vcexp_term(), vcexp_precedence_levels, process_vc_logic)

# parses the diferent tipes of possible terms of the verification condition expression
def vcexp_term():
    return vcexp_not() | \
           vcexp_relop() | \
           vcexp_group() | \
           vcexp_atom() | \
           quantifier_vc_logic() #|\
           #old_var_exp()

# parses the value true or false
def vcexp_atom():
    return (keyword('true') | keyword('false')) ^ (lambda parsed: AtomVCexp(parsed))

# parses the not keyword and uses it to create the negation of an expression
def vcexp_not():
    return keyword('not') + Lazy(vcexp_term) ^ (lambda parsed: NotVCexp(parsed[1]))

# used to parse first order logic
def vcexp_relop():
    relops = ['<', '<=', '>', '>=', '=', '!=']
    return aexp() + any_operator_in_list(relops) + aexp() ^ process_vc_relop

# used to parse grouping of expressions ( with the parentesis)
def vcexp_group():
    return keyword('(') + Lazy(vcexp) + keyword(')') ^ process_group

# returns the abstract sintatic tree of the verification condition expression parsed
def process_vc_relop(parsed):
    ((left, op), right) = parsed
    return RelopVCexp(op, left, right)

# parses the operators of the propositional logic
def process_vc_logic(op):
    if op == 'implies':
        return lambda l, r: ImpliesVCexp(l, r)
    if op == 'and':
        return lambda l, r: AndVCexp(l, r)
    elif op == 'or':
        return lambda l, r: OrVCexp(l, r)
    else:
        raise RuntimeError('unknown logic operator: ' + op)

# parses quantifiers of the first order logic
def quantifier_vc_logic():
    def process(parsed):
        ((((quantifier, vars),_), VCexp), _) = parsed
        return QuantifierVCExp(quantifier, vars, VCexp)
    return (keyword('forall') | keyword('exists')) + vars_exp() + keyword(':') + Lazy(vcexp) + keyword('end') ^ process


# parses a list of variable names used in the quantifiers, separated by semicolons
def vars_exp():
    separator = keyword(',') ^ (lambda x: lambda l, r: VarsExp(l, r))
    return Exp(var(), separator)

# def old_var_exp():
#     def process(parsed):
#         (((_, _), variable), _) = parsed
#         return OldVarExp(variable)
#     return keyword('Old') + keyword('(') + var() + keyword(')') ^ process

# parses a single variable name used in the previous function
def var():
    return (id  ^ (lambda v: VarVCexp(v)))

# the following functions are defined in the tutorial of the parser and will not be documented. See the tutorial for
# more info

# Boolean expressions
def bexp():
    return precedence(bexp_term(),
                      bexp_precedence_levels,
                      process_logic)

def bexp_term():
    return bexp_not()   | \
           bexp_relop() | \
           bexp_group()

def bexp_not():
    return keyword('not') + Lazy(bexp_term) ^ (lambda parsed: NotBexp(parsed[1]))

def bexp_relop():
    relops = ['<', '<=', '>', '>=', '=', '!=']
    return aexp() + any_operator_in_list(relops) + aexp() ^ process_relop

def bexp_group():
    return keyword('(') + Lazy(bexp) + keyword(')') ^ process_group

# Arithmetic expressions
def aexp():
    return precedence(aexp_term(),
                      aexp_precedence_levels,
                      process_binop)

def aexp_term():
    return aexp_value() | aexp_group()

def aexp_group():
    return keyword('(') + Lazy(aexp) + keyword(')') ^ process_group
           
def aexp_value():
    return (num ^ (lambda i: IntAexp(i))) | \
           (id  ^ (lambda v: VarAexp(v)))

# An IMP-specific combinator for binary operator expressions (aexp and bexp)
def precedence(value_parser, precedence_levels, combine):
    def op_parser(precedence_level):
        return any_operator_in_list(precedence_level) ^ combine
    parser = value_parser * op_parser(precedence_levels[0])
    for precedence_level in precedence_levels[1:]:
        parser = parser * op_parser(precedence_level)
    return parser

# Miscellaneous functions for binary and relational operators
def process_binop(op):
    return lambda l, r: BinopAexp(op, l, r)

def process_relop(parsed):
    ((left, op), right) = parsed
    return RelopBexp(op, left, right)

def process_logic(op):
    if op == 'and':
        return lambda l, r: AndBexp(l, r)
    elif op == 'or':
        return lambda l, r: OrBexp(l, r)
    else:
        raise RuntimeError('unknown logic operator: ' + op)

def process_group(parsed):
    ((_, p), _) = parsed
    return p

def any_operator_in_list(ops):
    op_parsers = [keyword(op) for op in ops]
    parser = reduce(lambda l, r: l | r, op_parsers)
    return parser

# Operator keywords and precedence level

aexp_precedence_levels = [
    ['*', '/'],
    ['+', '-'],
]

bexp_precedence_levels = [
    ['and'],
    ['or'],
]

vcexp_precedence_levels = [
    ['implies'],
    ['and'],
    ['or']
]
